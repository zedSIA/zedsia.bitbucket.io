var searchData=
[
  ['ui',['UI',['../_logger_8cs.html#a123f6a5cf3942de6a8e8680b196bd5eca71ff71526d15db86eb50fcac245d183b',1,'Logger.cs']]],
  ['usehammingdistance',['UseHammingDistance',['../namespace_fuzzy_string.html#a4137d7422ece8d03ad7f6f30f5c74797ad775446527cbb5e6f434dbc54a1014a9',1,'FuzzyString']]],
  ['usejaccarddistance',['UseJaccardDistance',['../namespace_fuzzy_string.html#a4137d7422ece8d03ad7f6f30f5c74797a52a65c9846d0daeb8496330be923c8e7',1,'FuzzyString']]],
  ['usejarodistance',['UseJaroDistance',['../namespace_fuzzy_string.html#a4137d7422ece8d03ad7f6f30f5c74797a0c8d0aba5450fa2625c604defe3b5b30',1,'FuzzyString']]],
  ['usejarowinklerdistance',['UseJaroWinklerDistance',['../namespace_fuzzy_string.html#a4137d7422ece8d03ad7f6f30f5c74797ad072244e95deee910e3464c4ee172a37',1,'FuzzyString']]],
  ['uselevenshteindistance',['UseLevenshteinDistance',['../namespace_fuzzy_string.html#a4137d7422ece8d03ad7f6f30f5c74797ad0dd78c1ee3e3d844f57dd844cc1ce8a',1,'FuzzyString']]],
  ['uselongestcommonsubsequence',['UseLongestCommonSubsequence',['../namespace_fuzzy_string.html#a4137d7422ece8d03ad7f6f30f5c74797adf579f893168acf13c503ed4351a30de',1,'FuzzyString']]],
  ['uselongestcommonsubstring',['UseLongestCommonSubstring',['../namespace_fuzzy_string.html#a4137d7422ece8d03ad7f6f30f5c74797a92b568891bab5be4a88744576808732a',1,'FuzzyString']]],
  ['usenormalizedlevenshteindistance',['UseNormalizedLevenshteinDistance',['../namespace_fuzzy_string.html#a4137d7422ece8d03ad7f6f30f5c74797aef5b4c98ca64df07f4b78ae33739ae37',1,'FuzzyString']]],
  ['useoverlapcoefficient',['UseOverlapCoefficient',['../namespace_fuzzy_string.html#a4137d7422ece8d03ad7f6f30f5c74797a4e3fc6896f98cfdf9d30d6efd9a1d8c2',1,'FuzzyString']]],
  ['useratcliffobershelpsimilarity',['UseRatcliffObershelpSimilarity',['../namespace_fuzzy_string.html#a4137d7422ece8d03ad7f6f30f5c74797a5badcecb78f4cfe47dd35597d6b9baf4',1,'FuzzyString']]],
  ['usesorensendicedistance',['UseSorensenDiceDistance',['../namespace_fuzzy_string.html#a4137d7422ece8d03ad7f6f30f5c74797a4fa788539f8c01ff3b24aada90465082',1,'FuzzyString']]],
  ['usetanimotocoefficient',['UseTanimotoCoefficient',['../namespace_fuzzy_string.html#a4137d7422ece8d03ad7f6f30f5c74797af56efe7c24ee9a17019b94967515bd8b',1,'FuzzyString']]]
];
