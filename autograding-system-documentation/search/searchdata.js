var indexSectionsWithContent =
{
  0: "_abcdefghijklmnopqrstuvw",
  1: "abcdefghijklmnoprstuvw",
  2: "dfglm",
  3: "abcdefghijklmnoprstuv",
  4: "abcdefghijlmnoprstuvw",
  5: "_abcdefghijklmnopqrstuvw",
  6: "cdfjklmprstu",
  7: "abcdefgijlmnoprstuvw",
  8: "abcdefghiklmnpqrstuvw",
  9: "o",
  10: "u",
  11: "u"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "namespaces",
  3: "files",
  4: "functions",
  5: "variables",
  6: "enums",
  7: "enumvalues",
  8: "properties",
  9: "events",
  10: "defines",
  11: "pages"
};

var indexSectionLabels =
{
  0: "All",
  1: "Classes",
  2: "Namespaces",
  3: "Files",
  4: "Functions",
  5: "Variables",
  6: "Enumerations",
  7: "Enumerator",
  8: "Properties",
  9: "Events",
  10: "Macros",
  11: "Pages"
};

